package app.endpoint.request;

import app.Currency;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeMoneyRequest {

    Long id;
    BigDecimal moneyToExchange;
    Currency currency;
}
