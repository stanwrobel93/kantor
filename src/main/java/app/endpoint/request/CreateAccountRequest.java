package app.endpoint.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateAccountRequest {

    private Long personalIdNo;
    private String nameAndSurname;
    private BigDecimal initialAccountPLN;
}
