package app.endpoint.response;

import lombok.Data;
import java.util.List;

@Data
public class BankResponse {
        List<Rates> rates;
}
