package app.endpoint.response;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Rates {
    BigDecimal mid;
}
