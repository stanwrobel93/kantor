package app.endpoint;

import app.endpoint.request.CreateAccountRequest;
import app.domain.account.entity.Account;
import app.domain.account.service.AccountService;
import app.endpoint.request.ExchangeMoneyRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/account")
public class AccountController {

    private final AccountService accountService;

    @PutMapping("/create")
    public void createAccount(@RequestBody CreateAccountRequest createAccountRequest) {
        accountService.createAccount(createAccountRequest);
    }

    @GetMapping("/{id}")
    public Account getAccount(@PathVariable("id") Long id) {
       return accountService.getAccount(id);
    }

    @PostMapping("/exchangeMoney")
    public void exchangePLN(@RequestBody ExchangeMoneyRequest exchangeMoneyRequest) {
        accountService.exchangeMoney(exchangeMoneyRequest);
    }
}
