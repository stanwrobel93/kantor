package app.domain.account.service;

import app.Currency;
import app.bank.BankCommunicator;
import app.endpoint.request.CreateAccountRequest;
import app.domain.account.entity.Account;
import app.domain.account.repository.AccountRepository;
import app.endpoint.request.ExchangeMoneyRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountService {

    private final AccountRepository accountRepository;
    private final BankCommunicator bankCommunicator;

    public void createAccount(CreateAccountRequest createAccountDto) {
        if (accountRepository.existsByPersonalIdNo(createAccountDto.getPersonalIdNo()) || !isAdult(createAccountDto.getPersonalIdNo())) {
            log.info("Account was not created"); //I don't want to use any secured information which could reveal somebody's account info
        } else {
            accountRepository.save(convertCreateAccToAcc(createAccountDto));
        }
    }

    public Account getAccount(Long id) {
        return accountRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Account with id %s was not found", id)));
    }

    private Boolean isAdult(Long personalIdNo) {
        int day = Integer.parseInt(personalIdNo.toString().substring(4, 6));
        int month = Integer.parseInt(personalIdNo.toString().substring(2, 4));
        int year = Integer.parseInt(personalIdNo.toString().substring(0, 2));
        if (month > 20) {
            year = year + 2000;
            month = month - 20;
        } else {
            year = year + 1900;
        }
        LocalDate birthDate = LocalDate.of(year, month, day);
        return birthDate.isBefore(LocalDate.now().minusYears(18));
    }

    private Account convertCreateAccToAcc(CreateAccountRequest createAccountDto) {
        return Account.builder()
                .nameAndSurname(createAccountDto.getNameAndSurname())
                .personalIdNo(createAccountDto.getPersonalIdNo())
                .accountInPLN(createAccountDto.getInitialAccountPLN()).build();
    }

    public void exchangeMoney(ExchangeMoneyRequest exchangeMoneyRequest) {
        Account account = accountRepository.findById(exchangeMoneyRequest.getId())
                .orElseThrow(() -> new RuntimeException(String.format("Account with id %s was not found", exchangeMoneyRequest.getId())));
        if(exchangeMoneyRequest.getCurrency().equals(Currency.USD)) {
            exchangeUSD(account, exchangeMoneyRequest.getMoneyToExchange());
        } else {
            exchangePLN(account, exchangeMoneyRequest.getMoneyToExchange());
        }
    }

    private void exchangeUSD(Account account, BigDecimal moneyToExchange) {
        boolean enoughMoney = account.getAccountInUSD().compareTo(moneyToExchange) >= 0;
        if(enoughMoney) {
            BigDecimal usdInPln = moneyToExchange.multiply(bankCommunicator.getUSDCurrencyRates());
            account.setAccountInUSD(account.getAccountInUSD().subtract(moneyToExchange));
            account.setAccountInPLN(account.getAccountInPLN().add(usdInPln));
            accountRepository.save(account);
        } else {
            log.info("Account " + account.getId() + " has not enough money");
        }
    }

    private void exchangePLN(Account account, BigDecimal moneyToExchange) {
        boolean enoughMoney = account.getAccountInPLN().compareTo(moneyToExchange) >= 0;
        if(enoughMoney) {
            BigDecimal plnInUsd = moneyToExchange.divide(bankCommunicator.getUSDCurrencyRates(), 2, RoundingMode.FLOOR);
            account.setAccountInPLN(account.getAccountInPLN().subtract(moneyToExchange));
            account.setAccountInUSD(account.getAccountInUSD().add(plnInUsd));
            accountRepository.save(account);
        } else {
            log.info("Account " + account.getId() + " has not enough money");
        }
    }
}
