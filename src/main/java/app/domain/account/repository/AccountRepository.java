package app.domain.account.repository;

import app.domain.account.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AccountRepository extends JpaRepository<Account, Long> {

    boolean existsByPersonalIdNo(Long personalIdNo);
}
