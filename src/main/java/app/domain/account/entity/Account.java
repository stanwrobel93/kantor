package app.domain.account.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ACCOUNT")
public class Account {

    @Id
    @Column(name = "ID") //I could make PersonalIdNo as ID but this... could be less secure
    private long id;

    @Column(name = "PERSONAL_ID")
    private long personalIdNo;

    @Column(name = "NAME_AND_SURNAME")
    private String nameAndSurname;

    @Column(name = "PLN_ACCOUNT_VALUE")
    private BigDecimal accountInPLN;

    @Column(name = "USD_ACCOUNT_VALUE")
    private BigDecimal accountInUSD;
}
