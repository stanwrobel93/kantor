package app.bank;

import app.endpoint.response.BankResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Component
@RequiredArgsConstructor
@Slf4j
public class BankCommunicator {
    String bankBaseUrl = "http://api.nbp.pl/api/exchangerates/rates/A/";
    String USD = "/USD/";

    public BigDecimal getUSDCurrencyRates() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        AtomicReference<BigDecimal> currencyRate = new AtomicReference<>();
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Void> entity = new HttpEntity<>(headers);

        String url = bankBaseUrl + USD;
        ResponseEntity<BankResponse> response = restTemplate.getForEntity(url, BankResponse.class, entity);
        if (response.getBody() != null) {
            response.getBody()
                    .getRates().stream()
                    .findFirst()
                    .ifPresent(rate -> currencyRate.set(rate.getMid()));
        } else {
            log.info("Bank is not responding");
        }
        return currencyRate.get();
    }
}
